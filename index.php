<?php get_header(); ?>

		<div id="content" class="tiles-container">
			<div id="inner-content" class="container">
				<main id="main" class="row" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf col-lg-6' ); ?> role="article">
					<div class="tile-container cf row">
						<header class="article-header cf">
							<?php if (has_post_thumbnail()) : ?>
								<div class="cf tile-image col-xs-12 col-sm-8 col-sm-push-4 col-md-8 col-md-push-4" style="background-image: url(
									<?php
										$thumbnail_id = get_post_thumbnail_id();
										$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
										echo $eye_img[0];
									?>);">
								</div>
							<?php else : ?>
							<?php endif; ?>

							<div class="cf tile-body col-xs-12 <?php if ( has_post_thumbnail() ) { echo 'col-sm-4 col-sm-pull-8 col-md-4 col-md-pull-8'; } ?>">
								<p class="header-category">
									<?php 
										$categories = get_the_category();
										$separator = ' ';
										$output = '';
										if ( $categories ) {
											foreach( $categories as $category ) {
												$output .= 
												'<span class="category' .
												$category->cat_ID .
												'">' .
												$category->name .
												'</span>' .
												$separator;
											}
										echo trim( $output, $separator );
										}
									?>
								</p>
								
								<time class="updated entry-time" datetime="<?php printf( get_the_time('Y-m-d') ) ?>" itemprop="datePublished">
									<i class="fa fa-calendar"></i>
									<?php
		              	printf( get_the_time(get_option('date_format')) );
	                ?>							
								</time>
										
								<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
							</div> <!-- .tile-body -->
						</header>
					</div> <!-- .tile-container -->
					<a href="<?php the_permalink() ?>" class="tile-link" title="<?php the_title_attribute(); ?>"></a>

				</article>

				<?php endwhile; ?>

				<?php bones_page_navi(); ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

				<?php endif; ?>
			</main>
		</div> <!-- #inner-content .container -->

		<?php get_footer(); ?>
