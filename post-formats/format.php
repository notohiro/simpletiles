
              <?php
                /*
                 * This is the default post format.
                 *
                 * So basically this is a regular post. if you don't want to use post formats,
                 * you can just copy ths stuff in here and replace the post format thing in
                 * single.php.
                 *
                 * The other formats are SUPER basic so you can style them as you like.
                 *
                 * Again, If you want to remove post formats, just delete the post-formats
                 * folder and replace the function below with the contents of the "format.php" file.
                */
              ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class('cf row'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

                <header class="article-header col-xs-12 col-md-10 col-md-offset-1">
	                
	                <p class="header-category">
										<?php 
											$categories = get_the_category();
											$separator = ' ';
											$output = '';
											if ( $categories ) {
												foreach( $categories as $category ) {
													$output .= 
													'<span class="category' .
													$category->cat_ID .
													'">' .
													$category->name .
													'</span>' .
													$separator;
												}
											echo trim( $output, $separator );
											}
										?>
									</p>
									
									<time class="updated entry-time" datetime="<?php printf( get_the_time('Y-m-d') ) ?>" itemprop="datePublished">
										<i class="fa fa-calendar"></i>
										<?php
			              	printf( get_the_time(get_option('date_format')) );
		                ?>							
									</time>

                  <h1 class="entry-title" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>

                </header> <?php // end article header ?>

                <section class="entry-content cf  col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2" itemprop="articleBody">
                  <?php
                    // the content (pretty self explanatory huh)
                    the_content();

                    /*
                     * Link Pages is used in case you have posts that are set to break into
                     * multiple pages. You can remove this if you don't plan on doing that.
                     *
                     * Also, breaking content up into multiple pages is a horrible experience,
                     * so don't do it. While there are SOME edge cases where this is useful, it's
                     * mostly used for people to get more ad views. It's up to you but if you want
                     * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
                     *
                     * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
                     *
                    */
                    wp_link_pages( array(
                      'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
                      'after'       => '</div>',
                      'link_before' => '<span>',
                      'link_after'  => '</span>',
                    ) );
                  ?>
                </section> <?php // end article section ?>

                <?php //comments_template(); ?>

              </article> <?php // end article ?>
